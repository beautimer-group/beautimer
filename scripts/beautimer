#!/usr/bin/python
# -*- coding: utf-8 -*-

# Copyright (C) 2011 Andrea Condoluci andreacondoluci@gmail.com

# This program is free software: you can redistribute it and/or modify it 
# under the terms of the GNU General Public License version 3, as published 
# by the Free Software Foundation.
# 
# This program is distributed in the hope that it will be useful, but 
# WITHOUT ANY WARRANTY; without even the implied warranties of 
# MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR 
# PURPOSE.  See the GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License along 
# with this program.  If not, see <http://www.gnu.org/licenses/>.

import sys
import time
import os, os.path
import gobject
import cairo, gtk, glib
from math import pi


DEBUG = False
if '--debug' in sys.argv:
    DEBUG = True

_UBUNTU_UNITY = False
try:
    from gi.repository import Unity#, GObject, Dbusmenu
    _UBUNTU_UNITY = True
except:
    pass

_BLUR_SUPPORT = False
try:
    import Image, ImageFilter, array
    _BLUR_SUPPORT = True
except:
    pass

_PYNOTIFY = False
try:
    import pynotify
    pynotify.init("BeauTimer")
    _PYNOTIFY = True
except:
    pass

import gettext
_ = lambda string: string

import gst
class GstPlayer:
    def __init__(self):
        self.looping = False
        self.player = gst.element_factory_make("playbin", "player")

        bus = self.player.get_bus()
        bus.enable_sync_message_emission()
        bus.add_signal_watch()
        bus.connect('message', self.on_message)
            
    def on_message(self, bus, message):
        if message.type == gst.MESSAGE_EOS:
            if self.looping:
                self.play()

    def set_location(self, location):
        self.player.set_property('uri', location)

    def seek0(self):
        self.player.seek_simple(gst.FORMAT_TIME, gst.SEEK_FLAG_FLUSH, 0)

    def play(self):
        self.seek0()
        self.looping = True
        self.player.set_state(gst.STATE_PLAYING)
        
    def stop(self):
        self.looping = False
        self.player.set_state(gst.STATE_NULL)


class Timer:
    STATE_TICKING = 0
    STATE_PAUSED  = 1
    STATE_STOPPED = 2
    STATE_ALARM   = 3
    
    is_snoozing = False
    
    STATE = STATE_STOPPED
    
    start_time  = 0
    last_paused = 0
    
    size = 200
    
    internal_radius = 0.0
    radius = 0.43
    
    # Angles
    zero = -pi/2.0
    start = zero
    end = zero
    
    
    dt = 0.0
    da = 0.015
    secs = 0
    
    def __init__ (self, preset=None):
        builder = gtk.Builder()
        builder.add_from_file('gui.xml')
        print builder.connect_signals(self)
        
        self.__getitem__ = lambda item: builder.get_object(item)
        
        self.first_init()
        
        if _UBUNTU_UNITY:
            self.launcher = Unity.LauncherEntry.get_for_desktop_id("beautimer.desktop")
        
        if _PYNOTIFY: #FIXME
            self.pynotification = pynotify.Notification( _("Timer over"), '', "dialog-warning")
            self.pynotification.set_urgency(pynotify.URGENCY_NORMAL)
        
        # Alarm player
        self.player = GstPlayer()
        alarm = 'file://' + os.path.abspath('alarm.wav')
        self.player.set_location(alarm)
        
        # Update spin buttons
        self['hours'].update()
        self['minutes'].update()
        self['seconds'].update()
        
        self.reset_countdown_label()
        
        gtk.gdk.notify_startup_complete()
        self['window'].show()
        
        if preset:
            t = preset.split(':')
            if t[0].isdigit() and t[1].isdigit():
                self['hours'].props.value = int(t[0])
                self['minutes'].props.value = int(t[1])
                self['seconds'].props.value = int(t[2])
                self['start'].clicked()
            else:
                print "Incorrect time format in launcher"
            
    
    def on_window_screen_changed(self, widget, event):
        screen = widget.get_screen()
        if screen.is_composited():
            colormap = screen.get_rgba_colormap()
        else:
            colormap = screen.get_rgb_colormap()
        widget.set_colormap(colormap)
    
    def first_init(self):
        widget = self['window']
        self.on_window_style_set(widget, None)
        self.on_window_screen_changed(widget, None)
        self.queue_draw()
    
    def main_quit(*args):
        return gtk.main_quit()
    
    #--- COLOR AND STYLE SETTINGS ---#
    def gtk_color_2_color(self, color):
        return (color.red_float, color.green_float, color.blue_float)
    
    def combine(self, color1, color2, k):
        r = []
        h = 1.0 - k
        for i in range(3):
            r.append(k*color1[i] + h*color2[i])
        return tuple(r)
    
    def on_window_style_set(self, window, event):
        self.background_color = self.gtk_color_2_color(window.style.bg[gtk.STATE_NORMAL])
        
        self.color = self.gtk_color_2_color(window.style.text[gtk.STATE_NORMAL])
        
        self.inactive_color = self.gtk_color_2_color(window.style.text[gtk.STATE_INSENSITIVE]) #self.combine(self.background_color, self.color, 0.4)
        self.background_track_color = self.combine(self.background_color, self.inactive_color, 0.6) + (0.8,)
        
        self.shadow = self.combine(self.color, self.background_track_color, 0.5)
        
        sens = self['spacer'].get_sensitive()
        self.reset_countdown_label()
        self['spacer'].set_sensitive(sens)
    
    def on_spin_output(self, spin):
        if self['minutes'].props.value == 0.0 == self['hours'].props.value == self['seconds'].props.value:
            spin.props.value = 1.0
        
        value = int(spin.props.value)
        if spin is self['seconds']:
            str = gettext.ngettext('second', 'seconds', value)
        elif spin is self['minutes']:
            str = gettext.ngettext('minute', 'minutes', value)
        else:
            str = gettext.ngettext('hour', 'hours', value)

        spin.props.text = '%d %s' % (value, str)
        return True
    
    def on_start_clicked(self, start):
        self.STATE = Timer.STATE_TICKING
        if self.is_snoozing:
            secs = 60
            if DEBUG:
                secs = 6
            M = 1
            H = 0
            S = 0
        else:
            H = self['hours'].props.value
            M = self['minutes'].props.value
            S = self['seconds'].props.value
            secs = ((H*60 + M)*60)+S
            if DEBUG:
                secs = 5
        
        self['spacer'].set_sensitive(True)
        self.set_countdown_label(int(H), int(M), int(S))
        
        self.secs = int(secs)
        self.interval = max(150, min(int((secs * (self.da/(2*pi)))*1000), 1000))
        print self.interval
        
        self.set_start_time()
        
        if not self.is_snoozing:
            pause = self['pause']
            pause.show()
            pause.grab_focus()
            self['stop'].show()
        
        self['hours'].hide(); self['minutes'].hide(); self['seconds'].hide()
        self['start'].hide()
        
        if _UBUNTU_UNITY:
            self.launcher.set_property("progress", 0.0)
            self.launcher.set_property("progress_visible", True)
        
        glib.timeout_add(self.interval, self.test)
        
        self.queue_draw()
    
    def set_start_time(self):
        self.start_time = time.time()
    
    def on_snooze_clicked(self, snooze):
        self.player.stop()
        
        snooze.hide()
        
        self['stop2'].show()
        self['stop2'].grab_focus()
        self['stop'].hide()
        self.is_snoozing = True
        self['start'].clicked()
        
        self.queue_draw()
    
    def on_continue_clicked(self, cont):
        self.STATE = Timer.STATE_TICKING
        cont.hide()
        
        self.start_time = time.time() - (self.last_paused - self.start_time)
        self.last_paused = 0
        
        self['spacer'].set_sensitive(True)
        self['pause'].show()
        self['pause'].grab_focus()
        
        
        glib.timeout_add(self.interval, self.test)
        
        self.queue_draw()
    
    def on_pause_clicked(self, pause):
        if self.STATE is not self.STATE_TICKING:
            return
        self.STATE = Timer.STATE_PAUSED
        
        self.last_paused = time.time()
        
        self['spacer'].set_sensitive(False)
        pause.hide()
        self['continue'].show()
        self['continue'].grab_focus()
        
        self.queue_draw()
    
    def on_stop_clicked(self, stop):
        self.STATE = Timer.STATE_STOPPED
        self.is_snoozing = False
        
        self.player.stop()
        
        # Gotta totally reset the initial configuration
        self['stop2'].hide(); stop.hide(); self['pause'].hide(); self['continue'].hide(); self['snooze'].hide()
        
        self['hours'].show(); self['minutes'].show(); self['seconds'].show()
        self['start'].show()
        self['start'].grab_focus()
        
        self.reset_countdown_label()
        
        if _UBUNTU_UNITY:
            self.launcher.set_property("progress_visible", False)
        
        self.start = self.end = self.zero
        self.last_time = 0
        self.secs = 0
        
        self.queue_draw()
    
    def reset_countdown_label(self):
        self['spacer'].set_sensitive(False)
        self['spacer'].set_text('-- : -- : --')
        self.update_internal_radius_size()
    
    def set_countdown_label(self, h, m, s):
        self['spacer'].set_text(str(h).zfill(2) + ':' + str(m).zfill(2) + ':' + str(s).zfill(2))
        self.update_internal_radius_size()
    
    def update_internal_radius_size(self):
        size = self['spacer'].get_layout().get_pixel_size()
        self.internal_radius = size[0]*1.0/self.size
        print self.internal_radius
    
    def notify_timer_end(self):
        #self['stop'].clicked()
        self.STATE = self.STATE_ALARM
        snooze = self['snooze']
        snooze.show(); snooze.grab_focus()
        
        self['stop2'].hide()
        self['pause'].hide()
        
        self['stop'].show()
        
        window = self['window']
        window.set_urgency_hint(True)
        window.present()
#        window.set_keep_above(True)
        
        self.player.play()
        
        if 0 and _PYNOTIFY: # FIXME
            self.pynotification.show()
    
    def test(self):
        gtk.gdk.threads_enter()
        is_ticking = self.STATE is Timer.STATE_TICKING
        if is_ticking:
            # Calculate Elapsed time
            elaspsed_sec = time.time() - self.start_time
            
            # Calculate elapsed time
            total_sec_left = self.secs - elaspsed_sec
            
            i_total_sec_left = int(total_sec_left)
            seconds_in_hour = 60

            # >>> m,s = divmod(7401, 60)
            # >>> h,m = divmod(m, 60)
            # >>> print "%d:%02d:%02d" % (h, m, s)
            # 2:03:21

            mins_left,sec_left = divmod(i_total_sec_left, seconds_in_hour)
            hours_left,mins_left = divmod(mins_left, seconds_in_hour)
            
            self.set_countdown_label(hours_left, mins_left, sec_left)
            
            # Update angle
            frac = 1.0 - total_sec_left / self.secs
            if _UBUNTU_UNITY:
                self.launcher.set_property("progress", frac)
#            print frac
            
            self.start = pi * (2.0*frac - 0.5)
            if self.start >= 1.5*pi: # the end!
                is_ticking = False
                self.notify_timer_end()
            
            #self.queue_draw() # Don't need because updating label will make background redraw
        gtk.gdk.threads_leave()
        return is_ticking
    
    queue_draw = lambda self: self['window'].queue_draw()
    
    def on_window_expose_event(self, widget, event):
        cr = widget.window.cairo_create()
        
        # Restrict Cairo to the exposed area; avoid extra work
        cr.rectangle(event.area.x, event.area.y,
                event.area.width, event.area.height)
        cr.clip()
        
        cr.set_operator(cairo.OPERATOR_SOURCE)
        cr.set_source_rgb(*self.background_color)
#        cr.set_source_rgba(0,0,0,0)
        cr.paint()
        
        cr.scale(self.size, self.size)
        self.ctx_draw_timer(cr, wow=True)
        if self.STATE is self.STATE_TICKING:
            cr.set_source_rgb(*self.color)
        else:
            cr.set_source_rgb(*self.inactive_color)
        cr.fill()
        
        if (self.STATE is self.STATE_TICKING and self.start == self.end) or self.STATE is self.STATE_STOPPED:
            return False

        self.ctx_draw_timer(cr)
        cr.clip()
        
        if _BLUR_SUPPORT and self.STATE in (self.STATE_TICKING, self.STATE_ALARM): #FIXME
            cr.scale(1.0/self.size, 1.0/self.size)
            s = self.build_shadow()
            cr.set_source_surface(s, 0, 0)
        else:
            cr.set_source_rgb(*self.background_track_color[:3])
        cr.paint()
        
        return False
    
#    do_paint = lambda self, widget, event: expose(widget, event)
    
    def ctx_draw_timer(self, context, radius=radius, special=0.0, transl=0.0, wow=False, gaga=0.0):
        if self.start == self.end or wow or self.STATE is self.STATE_ALARM:
            start = 2*pi
            end = 0
            special = 0
        else:
            (start, end) = (self.start, self.end)
        context.arc_negative(0.5 + transl, 0.5 + transl, radius-gaga, start + special, end - special)
        #context.line_to(0.5 + transl, 0.5 + transl)
        context.arc(0.5 + transl, 0.5 + transl, self.internal_radius+gaga, end-special, start + special)
        context.arc_negative(0.5 + transl, 0.5 + transl, radius-gaga, start + special, start + special)

    def srf_blur(self, surface):
        im = Image.frombuffer("RGBA", (self.size, self.size), surface.get_data(), "raw", "RGBA", 0, 1)

        im = im.filter(ImageFilter.BLUR)
        im = im.filter(ImageFilter.BLUR)
#        im = im.filter(ImageFilter.SMOOTH_MORE) 

        imgd = im.tostring("raw", "RGBA", 0, 1)
        a = array.array('B', imgd)

        return cairo.ImageSurface.create_for_data(a, cairo.FORMAT_ARGB32, self.size, self.size, self.size*4) # last is stride: what??? FIXME


    def build_shadow(self):
        surface = cairo.ImageSurface(cairo.FORMAT_ARGB32, self.size, self.size)
        ctx = cairo.Context(surface)
        ctx.scale (self.size/1.0, self.size/1.0)
        
        ctx.set_operator(cairo.OPERATOR_SOURCE)
        ctx.set_source_rgb(*self.shadow)
        ctx.paint()
        
        special = 0.04
        if self.start - self.end > 2*special:
            ctx.new_path()
            ctx.set_source_rgba(*self.background_track_color)
            self.ctx_draw_timer(ctx, gaga=0.015, special=-special, transl=0.002)
            ctx.fill()
            del ctx
            return self.srf_blur(surface)
        else:
            del ctx
            return surface


if __name__ == '__main__':
    abspath = os.path.abspath(sys.argv[0])
    dname = os.path.dirname(abspath)
    share = os.path.join(dname, '../share/beautimer/')
    if DEBUG:
        share = os.path.join(dname, '../data/')
    os.chdir(share)
    
    gtk.gdk.threads_init(); gobject.threads_init()
    
    preset = None
    for i in sys.argv[1:]:
        if ':' in i:
            preset = i
            break
    
    gtk.gdk.threads_enter()
    Timer(preset)
    gtk.main()
    gtk.gdk.threads_leave()
