# Beautimer
Beautimer is a quick and easy to use countdown timer, which allows users to count down hours and minutes of your choice. When the countdown is over, an alarm sound will be played automatically.

## Ubuntu 11.x - 14.x support

This is only works in older gtk and gstream targets for X11 linux based desktops.

## Docker

There is an expermental dockerfile in 

https://gitlab.com/beautimer-group/beautimer-docker

And an attached docker build in the gitlab registry under this archieve

`docker pull registry.gitlab.com/beautimer-group/beautimer`

**Note**: Jack server will likely conflict with pulse audio at this time 