#!/usr/bin/env python

from distutils.core import setup

setup(name='BeauTimer',
      version='0.2.1',
      license='GPL-3',
      description='A Beautiful Ubuntu Countdown Timer',
      author='Andrea Condoluci',
      author_email='andreacondoluci@gmail.com',
      url='https://launchpad.net/beautimer',
      scripts=['scripts/beautimer'],
      data_files=[
                ('share/beautimer/', ['data/gui.xml', 'data/alarm.wav']),
                ('share/icons/hicolor/scalable/apps/', ['data/beautimer.svg']),
                ('share/icons/hicolor/48x48/apps/', ['data/beautimer.png']),
                ('share/applications/', ['data/beautimer.desktop'])
                ],
     )
